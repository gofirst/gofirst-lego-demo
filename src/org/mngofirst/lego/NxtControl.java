package org.mngofirst.lego;

import org.mngofirst.lego.Command.CmdException;

/**
 * Provides high-level methods for controlling the NXT.
 * @author Bryan Herbst
 *
 */
public class NxtControl {
	// Movement constants
	private static final int LOW_SPEED = 100;
	private static final int SPEED_STOP = 0;
	private static final int TURN_RATIO = 0;
	
	// Sensor ports
	public static final byte TOUCH_SENSOR_PORT = Command.PORT_4;
	public static final byte RANGE_SENSOR_PORT = Command.PORT_3;
	public static final byte LIGHT_SENSOR_PORT = Command.PORT_2;
	private static final byte[] SENSORS = {TOUCH_SENSOR_PORT, RANGE_SENSOR_PORT, LIGHT_SENSOR_PORT};
	
	
	// Light sensor color modes
	public static final byte COLOR_RED = Command.SENSOR_LIGHT_RED;
	public static final byte COLOR_GREEN = Command.SENSOR_LIGHT_GREEN;
	public static final byte COLOR_BLUE = Command.SENSOR_LIGHT_BLUE;
	public static final byte COLOR_FULL = Command.SENSOR_LIGHT_FULL;
	public static final byte COLOR_NONE = Command.SENSOR_LIGHT_NONE;
	/**
	 * Move forward indefinitely at a constant speed 
	 */
	public static void forward() {
		try {
			byte[] left = Command.genMotorCmd(false, Command.PORT_A, LOW_SPEED, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			byte[] right = Command.genMotorCmd(false, Command.PORT_B, LOW_SPEED, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			
			BluetoothThread.getThread().write(combine(left, right));
		} catch (CmdException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Move backward indefinitely at a constant speed
	 */
	public static void backward() {
		try {
			byte[] left = Command.genMotorCmd(false, Command.PORT_A, -LOW_SPEED, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			byte[] right = Command.genMotorCmd(false, Command.PORT_B, -LOW_SPEED, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			
			BluetoothThread.getThread().write(combine(left, right));
		} catch (CmdException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Rotate left indefinitely at a constant speed
	 */
	public static void left() {
		try {
			byte[] left = Command.genMotorCmd(false, Command.PORT_A, -LOW_SPEED, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			byte[] right = Command.genMotorCmd(false, Command.PORT_B, LOW_SPEED, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			
			BluetoothThread.getThread().write(combine(left, right));
		} catch (CmdException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Rotate right indefinitely at a constant speed
	 */
	public static void right() {
		try {
			byte[] left = Command.genMotorCmd(false, Command.PORT_A, LOW_SPEED, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			byte[] right = Command.genMotorCmd(false, Command.PORT_B, -LOW_SPEED, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			
			BluetoothThread.getThread().write(combine(left, right));
		} catch (CmdException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Activate shooting motor indefinitely
	 */
	public static void shoot() {
		try {
			byte[] cmd = Command.genMotorCmd(false, Command.PORT_C, LOW_SPEED, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			
			BluetoothThread.getThread().write(cmd);
		} catch (CmdException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Set the left motor to a specified speed indefinitely
	 * @param speed - Speed at which to move motor (-100, 100)
	 */
	public static void setLeftMotor(int speed) {
		try {
			byte[] left = Command.genMotorCmd(false, Command.PORT_A, speed, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			
			BluetoothThread.getThread().write(left);
		} catch (CmdException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set the right motor to a specified speed indefinitely
	 * @param speed - Speed at which to move motor (-100, 100)
	 */
	public static void setRightMotor(int speed) {
		try {
			byte[] right = Command.genMotorCmd(false, Command.PORT_B, speed, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			
			BluetoothThread.getThread().write(right);
		} catch (CmdException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Stop all motors
	 */
	public static void stop() {
		try {
			byte[] cmd = Command.genMotorCmd(false, Command.PORT_ALL, SPEED_STOP, Command.OUT_MODE_ON, Command.REG_MODE_SPEED, 
					TURN_RATIO, Command.RUN_STATE_RUNNING, Command.FOREVER);
			
			BluetoothThread.getThread().write(cmd);
		} catch (CmdException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Initialize all of the NXT's sensors
	 */
	public static void initSensors() {
		byte[] touchCmd = Command.genSetInputModeCmd(TOUCH_SENSOR_PORT, Command.SENSOR_SWITCH, Command.SENSOR_MODE_BOOL);
		byte[] rangeCmd = Command.genSetInputModeCmd(RANGE_SENSOR_PORT, Command.SENSOR_CUSTOM, Command.SENSOR_MODE_RAW);
		byte[] lightCmd = Command.genSetInputModeCmd(LIGHT_SENSOR_PORT, Command.SENSOR_LIGHT_NONE, Command.SENSOR_MODE_RAW);
		byte[] cmd = combine(touchCmd, rangeCmd);
		cmd = combine(cmd, lightCmd);
		BluetoothThread.getThread().write(cmd);
	}
	
	public static void setLight(byte colorMode) {
		byte[] cmd = Command.genSetInputModeCmd(LIGHT_SENSOR_PORT, colorMode, Command.SENSOR_MODE_RAW);
		BluetoothThread.getThread().write(cmd);
	}
	
	/**
	 * Send a request to the NXT for data from all sensors
	 */
	public static void updateSensors() {
		for (byte sensor: SENSORS) {
			BluetoothThread.getThread().write(Command.genGetInputCmd(sensor));
		}
	}
	
	/**
	 * Combine a command (byte[]) for the left side of the robot with 
	 * a command for the right side of the robot
	 * @param left - Left side command byte[]
	 * @param right - Right side command byte[]
	 * @return cmd - The combined command
	 */
	private static byte[] combine(byte[] left, byte[] right) {
		byte[] combined = new byte[left.length + right.length];
		System.arraycopy(left, 0, combined, 0, left.length);
		System.arraycopy(right, 0, combined, left.length, right.length);
		return combined;
	}
	
}
