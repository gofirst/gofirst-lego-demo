package org.mngofirst.lego;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

/**
 * Contains a Fragment that allows the user to interact with the NXT.
 * Also contains the tabbed navigation
 * @author Bryan Herbst
 *
 */
public class ControlActivity extends FragmentActivity implements ActionBar.TabListener {

    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    
    // Tab positions
    private static final int TAB_CONTROL = 0;
    private static final int TAB_ADVANCED = 1;
    private static final int TAB_SENSORS = 2;
    
	// Bluetooth constants for dialog callbacks
	private final int REQUEST_ENABLE_BT = 7;
	private final int REQUEST_CONNECT_DEVICE = 8;
	
	// Bluetooth vars
	BluetoothAdapter mBluetoothAdapter;
	BluetoothThread btThread;
	boolean connected = false;
	
	// Buttons
	Button upBtn;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // For each of the sections in the app, add a tab to the action bar.
        actionBar.addTab(actionBar.newTab().setText(R.string.title_control).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.title_advanced).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.title_sensors).setTabListener(this));
        
        // Initialize Bluetooth
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
        	Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	findAndConnectDevice(false);
    }
    
    @Override
    public void onStop() {
    	super.onStop();
    	if (btThread != null)
    		btThread.cancel();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch(item.getItemId()) {
    		case R.id.menu_reconnect: findAndConnectDevice(true); return true;
    		default: break;
    	}
    	return false;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity, menu);
        return true;
    }
    
    /*
     * (Should only be) called when the Bluetooth connection activities return
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	switch(requestCode) {
	    	case REQUEST_CONNECT_DEVICE: 
	    		if(resultCode != Activity.RESULT_OK)
	    			return;
	    		connectDevice(data);
				connected = true;
				break;
	    	case REQUEST_ENABLE_BT: 
	    		Intent selectDeviceIntent = new Intent(this, DeviceListActivity.class);
		        startActivityForResult(selectDeviceIntent, REQUEST_CONNECT_DEVICE);
	    		break;
    	}
    	
    	// Not sure why, but after the DeviceListActivity returns, buttons don't work unless you switch tabs
    }
    
    /**
     * Creates an intent to launch the DeviceListActivity, which will search for bluetooth devices
     * and prompt the user to connect to one of the devices.
     * 
     * If bluetooth is not enabled, this will first prompt theuser to enable bluetooth
     * @param reconnect - Whether or not to force a new connection to be made
     */
    private void findAndConnectDevice(boolean reconnect) {
    	if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    	// If the device is already connected, don't reconnect
    	// Unless the reconnect flag is specified
    	if(!connected || reconnect) {
		    Intent selectDeviceIntent = new Intent(this, DeviceListActivity.class);
		    startActivityForResult(selectDeviceIntent, REQUEST_CONNECT_DEVICE);
    	}
    }
    
    /**
     * Connect to a bluetooth device 
     * @param data - Intent containing the Bluetooth device info
     */
    private synchronized void connectDevice(Intent data){
    	 String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
    	 BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
    	 btThread = new BluetoothThread(device);
    	 btThread.start();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getActionBar().setSelectedNavigationItem(
                    savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM,
                getActionBar().getSelectedNavigationIndex());
    }

    

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    	// When the given tab is selected, show the tab contents in the container
    	Fragment fragment = null;
        switch(tab.getPosition()) {
        	case TAB_CONTROL: fragment = new ControlFrag(); break;
        	case TAB_ADVANCED: fragment = new AdvancedControlFrag(); break;
        	case TAB_SENSORS: fragment = new SensorsFrag(); break;
        	default: fragment = new ControlFrag(); break;
        }
         
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }
}
