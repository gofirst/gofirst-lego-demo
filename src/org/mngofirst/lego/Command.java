package org.mngofirst.lego;

import java.nio.ByteBuffer;

/**
 * Command class, used to generated NXT commands
 * Contains low(er)-level methods to generate commands.
 * 
 * Command packet format:
 * [type]               -- eg direct command, system command, reply
 * [operation]          -- eg set output, start program
 * <payload>            -- Operation Arguments
 * 
 * Reply packet format:
 * [type]               -- Reply (0x02)
 * [operation]          -- eg set output, start program
 * [status]     		-- 0x00 on sucess, error type otherwise
 * <payload>            -- The reply
 * 
 * @author Bryan Herbst
 */
public class Command {
	// Byte 0 - Message type
	public static final byte DIRECT_CMD = 0x00;
	public static final byte SYS_CMD = 0x01;
	public static final byte RESPONSE = 0x02;
	public static final byte DIRECT_CMD_NORESP = (byte)0x80;
	public static final byte SYS_CMD_NORESP = (byte)0x81;
	
	// Byte 1 - Operation
	public static final byte OP_STARTPROGRAM = 0x00;
	public static final byte OP_STOPPROGRAM = 0x01;
	public static final byte OP_PLAYSOUNDFILE = 0x02;
	public static final byte OP_PLAYTONE = 0x03;
	public static final byte OP_SETOUTPUT = 0x04;
	public static final byte OP_SETINPUTMODE = 0x05;
	public static final byte OP_GETOUTPUTSTATE = 0x06;
	public static final byte OP_GETINPUTVALUES = 0x07;
	public static final byte OP_RESETINPUTSCALEDVALUE = 0x08;
	public static final byte OP_MESSAGEWRITE = 0x09;
	public static final byte OP_RESETMOTORPOSITION = 0x0A;
	public static final byte OP_GETBATTERYLEVEL = 0x0B;
	public static final byte OP_STOPSOUNDPLAYBACK = 0x0C;
	public static final byte OP_KEEPALIVE = 0x0D;
	public static final byte OP_LSFETSTATUS = 0x0E;
	public static final byte OP_LSWRITE = 0x0F;
	public static final byte OP_LSREAD = 0x10;
	public static final byte OP_GETCURRENTPROGRAMNAME = 0x11;
	public static final byte OP_MESSAGEREAD = 0x13;
	
	// Byte 2 (in a response packet)
	public static final byte STATUS_SUCESS = 0x00;
	public static final byte ERR_TX_PENDING = 0x20;
	public static final byte ERR_QUEUE_EMPTY = 0x40;
	public static final byte ERR_REQ_FAILED = (byte)0xBD;
	public static final byte ERR_UNKNOWN_OP = (byte)0xBE;
	public static final byte ERR_INSANE_PKT = (byte)0xBF;
	public static final byte ERR_DATA_RANGE = (byte)0xC0;
	public static final byte ERR_COMM_BUS = (byte)0xDD;
	public static final byte ERR_COMM_MEM_FULL = (byte)0xDE;
	public static final byte ERR_CONN_INVALID = (byte)0xDF;
	public static final byte ERR_CONN_BUSY = (byte)0xE0;
	public static final byte ERR_NO_ACTIVE_PROG = (byte)0xEC;
	public static final byte ERR_ILLEGAL_SIZE = (byte)0xED;
	public static final byte ERR_IlLEGAL_MAILBOX = (byte)0xEE;
	public static final byte ERR_INVALID_FIELD = (byte)0xEF;
	public static final byte ERR_BAD_PORT = (byte)0xF0;
	public static final byte ERR_MEM_FULL = (byte)0xFB;
	public static final byte ERR_BAD_ARGS = (byte)0xFF;
	
	// Output ports
	public static final byte PORT_A = 0x00;
	public static final byte PORT_B = 0x01;
	public static final byte PORT_C = 0x02;
	public static final byte PORT_ALL = (byte)0xFF;
	
	// Input ports
	public static final byte PORT_1 = 0x00;
	public static final byte PORT_2 = 0x01;
	public static final byte PORT_3 = 0x02;
	public static final byte PORT_4 = 0x03;
	
	// Output modes
	public static final byte OUT_MODE_ON = 0x01;
	public static final byte OUT_MODE_BRAKE = 0x02;
	public static final byte OUT_MODE_REGULATED = 0x04;
	
	// Output regulation modes
	public static final byte REG_MODE_IDLE = 0x00;
	public static final byte REG_MODE_SPEED = 0x01;
	public static final byte REG_MODE_SYNC = 0x02;
	
	// Output run state
	public static final byte RUN_STATE_IDLE = 0x00;
	public static final byte RUN_STATE_RAMPUP = 0x10;
	public static final byte RUN_STATE_RUNNING = 0x20;
	public static final byte RUN_STATE_RAMPDOWN = 0x40;
	
	// Output power max/min
	public static final int OUT_MAX = 100;
	public static final int OUT_MIN = -100;
	
	// Turn ratio max/min
	public static final int TURN_MAX = 100;
	public static final int TURN_MIN = -100;
	
	// Time to run = forever
	public static final int FOREVER = 0x00;
	
	// Sensor types 
	public static final byte SENSOR_NONE = 0x00;
	public static final byte SENSOR_SWITCH = 0x01;
	public static final byte SENSOR_TEMP = 0x02;
	public static final byte SENSOR_REFLECTION = 0x03;
	public static final byte SENSOR_ANGLE = 0x04;
	public static final byte SENSOR_LIGHT_ACTIVE = 0x05; // Legacy light sensor- flood light on?
	public static final byte SENSOR_LIGHT_INACTIVE = 0x06; // Legacy light sensor- flood light off?
	public static final byte SENSOR_SOUND_DB = 0x07;
	public static final byte SENSOR_SOUND_DBA = 0x08;
	public static final byte SENSOR_CUSTOM = 0x09;
	public static final byte SENSOR_LOWSPEED = 0x0A;
	public static final byte SENSOR_LOWSPEED_9V = 0x0B;
	// 0x0C is old NUM_SENSORS ?
	public static final byte SENSOR_LIGHT_FULL = 0x0D;
	public static final byte SENSOR_LIGHT_RED = 0x0E;
	public static final byte SENSOR_LIGHT_GREEN = 0x0F;
	public static final byte SENSOR_LIGHT_BLUE = 0x10;
	public static final byte SENSOR_LIGHT_NONE = 0x11;
	
	// Sensor modes
	public static final byte SENSOR_MODE_RAW = 0x00;
	public static final byte SENSOR_MODE_BOOL = 0x20;
	public static final byte SENSOR_MODE_TRANSITION_CNT = 0x40;
	public static final byte SENSOR_MODE_PERIOD_CNT = 0x60;
	public static final byte SENSOR_MODE_PCTFULLSCALE = (byte)0x80;
	public static final byte SENSOR_MODE_CELSIUS = (byte)0xA0;
	public static final byte SENSOR_MODE_FAHRENHEIT = (byte)0xC0;
	public static final byte SENSOR_MODE_ANGLESTEP = (byte)0xE0;
	public static final byte SENSOR_MODE_SLOPEMASK = (byte)0x1F;
	public static final byte SENSOR_MODE_MODEMASK = (byte)0xE0;
	
	
	/**
	 * Generate a command (byte[]) to move one or more motors
	 * @param requireResponse - Whether to require a response from the NXT
	 * @param port - Which port the desired motor is connected to
	 * @param power - Power level (-100, 100)
	 * @param mode - Output mode (OUT_MODE_[X])
	 * @param regulation - Regulation mode (REG_MODE_[X])
	 * @param turnRatio - Turn ratio (-100, 100)
	 * @param runState - Run state (RUN_STATE[X])
	 * @param time - How long to run motor (0 = forever)
	 * @return The generated command
	 * @throws CmdException
	 */
	public static byte[] genMotorCmd(boolean requireResponse, byte port, int power, byte mode,
			byte regulation, int turnRatio, byte runState, int time) throws CmdException {
		if (power > OUT_MAX || power < OUT_MIN) {
			throw new CmdException("Power out of acceptable range ("+OUT_MIN+", "+OUT_MAX+")");
		}
		if (turnRatio > TURN_MAX || turnRatio < TURN_MIN) {
			throw new CmdException("Turn ratio out of acceptable range ("+TURN_MIN+", "+TURN_MAX+")");
		}
		
		byte cmdType = DIRECT_CMD;
		if (!requireResponse) {
			cmdType = DIRECT_CMD_NORESP;
		}
		

		byte[] timeArr = ByteBuffer.allocate(4).putInt(time).array();
		byte[] cmd = {0x00, 0x00, cmdType, OP_SETOUTPUT, port, (byte)power, mode, regulation, (byte)turnRatio, runState, timeArr[0], timeArr[1], timeArr[2], timeArr[3]};
		byte[] header = getHeader(cmd);
		cmd[0] = header[0];
		cmd[1] = header[1];
		return cmd;
	}
	
	/**
	 * Generate a command to request a input value
	 * @param port - The input port to request
	 * @return The generated command
	 */
	public static byte[] genGetInputCmd(byte port) {
		byte[] cmd = {0x00, 0x00, DIRECT_CMD, OP_GETINPUTVALUES, port};
		byte[] header = getHeader(cmd);
		cmd[0] = header[0];
		cmd[1] = header[1];
		return cmd;
	}
	
	public static byte[] genSetInputModeCmd(byte port, byte type, byte mode) {
		byte[] cmd = {0x00, 0x00, DIRECT_CMD, OP_SETINPUTMODE, port, type, mode};
		byte[] header = getHeader(cmd);
		cmd[0] = header[0];
		cmd[1] = header[1];
		return cmd;
	}
	
	/**
	 * Generate a header for a given message
	 * @param msg - The message to generate a header for
	 * @return The header, a two byte array
	 */
	public static byte[] getHeader(byte[] msg) {
		byte[] hdr = {(byte)(msg.length-2), 0x00};
		return hdr;
	}
	
	
	/**
	 * Generic exception generated by trying to create a command for the NXT
	 * 
	 * @author Bryan Herbst
	 *
	 */
	public static class CmdException extends Exception {
		//Auto generated UID
		private static final long serialVersionUID = -1569769667710500075L;
		
		String error;
		
		public CmdException(String error) {
			super(error);
		}
	}
}
