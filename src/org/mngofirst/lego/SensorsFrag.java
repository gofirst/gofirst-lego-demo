package org.mngofirst.lego;

import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

/**
 * Advanced NXT control Fragment.
 * @author Bryan Herbst
 *
 */
public class SensorsFrag extends Fragment implements  BluetoothThread.BluetoothMsgListener, OnCheckedChangeListener {
	// Reply message indicies
	private final static int REPLY_PORT = 5;
	private final static int REPLY_VALID = 6;
	private final static int REPLY_TYPE = 8;
	private final static int REPLY_VALUE = 10;
	private final static int REPLY_COLOR = 14;
	
	// Color values
	private final int[] LIGHT_REPLY_COLORS = {Color.BLACK, Color.BLUE, Color.GREEN, Color.YELLOW, Color.RED, Color.WHITE };
	private final int[] LIGHT_REPLY_TEXT_COLORS = {Color.WHITE, Color.WHITE, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK };
	private final String[] LIGHT_REPLY_STRINGS = {"Black", "Blue", "Green", "Yellow", "Red", "White"};
	
	// Sensor values
	private int lightValue = 0;
	
	// Byte booleans
	private final static byte FALSE = (byte)0xFF;
	
	SensorUpdater updateThread;
	
	// UI Elements
	TextView touchText, rangeText, lightText;
	RadioGroup lightModeGroup;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return inflater.inflate(R.layout.sensors_frag, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		touchText = (TextView)getView().findViewById(R.id.text_touch);
		rangeText = (TextView)getView().findViewById(R.id.text_range);
		lightText = (TextView)getView().findViewById(R.id.text_light);
		lightModeGroup = (RadioGroup)getView().findViewById(R.id.lightModeGroup);
		
		lightModeGroup.setOnCheckedChangeListener(this);

		NxtControl.initSensors();
		BluetoothThread.setBluetoothMsgListener(this);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		updateThread = new SensorUpdater();
		updateThread.start();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		updateThread.pause();
		updateThread = null;
	}
	
	@Override
	public void onMessageReceived(final byte[] msg) {
		if(msg.length < 3 || msg[3] != Command.OP_GETINPUTVALUES|| msg[REPLY_VALID] == FALSE) 
			return;
		// Since this method is called by the Bluetooth thread and it updates the UI (textviews),
		// We need to use runOnUiThread()
		this.getActivity().runOnUiThread(new Runnable() {
		    public void run() {
				switch (msg[REPLY_PORT]) {
					case NxtControl.TOUCH_SENSOR_PORT:
						if(msg[REPLY_VALUE] == FALSE) {
							touchText.setText(R.string.touch_not_pressed);
							touchText.setBackgroundColor(Color.TRANSPARENT);
						} else {
							touchText.setText(R.string.touch_pressed);
							touchText.setBackgroundColor(Color.GREEN);
						}
						break;
					case NxtControl.LIGHT_SENSOR_PORT:
						if(msg[REPLY_TYPE] == Command.SENSOR_LIGHT_FULL) {
							updateLightColorText(msg[REPLY_COLOR]);
						} else {
							lightValue = replyDataToInt(msg);
							lightText.setText(getActivity().getString(R.string.light_prefix) + " " + lightValue);
						}
						break;
					case NxtControl.RANGE_SENSOR_PORT:
						rangeText.setText(getActivity().getString(R.string.range_prefix) + " " + msg[REPLY_VALUE]);
					break;
					default: break;
				}
		    }
		});
		
	}
	
	private void updateLightColorText(byte color){
		int colorIndex = (int)color - 1; // 0x1 = int 2 = BLACK
		lightText.setBackgroundColor(LIGHT_REPLY_COLORS[colorIndex]);
		lightText.setTextColor(LIGHT_REPLY_TEXT_COLORS[colorIndex]);
		lightText.setText(getActivity().getString(R.string.light_prefix) + " " + LIGHT_REPLY_STRINGS[colorIndex]);
	}
	
	/**
	 * Given a message, returns a float representation.
	 * 
	 * @param msg the message received from the NXT
	 * @return
	 */
	private int replyDataToInt(byte[] msg) {
		// To convert, we take two little-endian ordered bytes from the message, 
		// convert them into an int, then that into a float.
	    return ( (msg[REPLY_VALUE + 1] << 8) | msg[REPLY_VALUE] );
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// Reset light sensor text background
		lightText.setBackgroundColor(Color.TRANSPARENT);
		lightText.setTextColor(Color.BLACK);
		switch(checkedId) {
			case R.id.lightModeOffButton:
				NxtControl.setLight(NxtControl.COLOR_NONE);
				break;
			case R.id.lightModeFullButton:
				NxtControl.setLight(NxtControl.COLOR_FULL);
				break;
			case R.id.lightModeRedButton:
				NxtControl.setLight(NxtControl.COLOR_RED);
				break;
			case R.id.lightModeGreenButton:
				NxtControl.setLight(NxtControl.COLOR_GREEN);
				break;
			case R.id.lightModeBlueButton:
				NxtControl.setLight(NxtControl.COLOR_BLUE);
				break;
		}
	}
	
	
	/**
	 * Handles periodic updating of the sensors
	 *
	 */
	public class SensorUpdater extends Thread {
		private final static int REFRESH_TIME = 1700;
		
		boolean running = true;
		
		@Override
		public void run() {
			while (running) {
				NxtControl.updateSensors();
				try {
					Thread.sleep(REFRESH_TIME);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		public void pause() {
			running = false;
		}
	}
}