package org.mngofirst.lego;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * Simple NXT control Fragment
 * @author Bryan Herbst
 *
 */
public class ControlFrag extends Fragment implements OnTouchListener{
	ImageButton btnForward, btnLeft, btnRight, btnBackward;
	Button btnShoot;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return inflater.inflate(R.layout.control_frag, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Get button views
	    btnForward = (ImageButton)getView().findViewById(R.id.btn_fwd);
	    btnBackward = (ImageButton)getView().findViewById(R.id.btn_back);
	    btnRight = (ImageButton)getView().findViewById(R.id.btn_right);
	    btnLeft = (ImageButton)getView().findViewById(R.id.btn_left);
	    btnShoot = (Button)getView().findViewById(R.id.btn_shoot);
	    

	}
	
	@Override
	public void onResume() {
		super.onResume();
	    // Set button listener
	    btnForward.setOnTouchListener(this);
	    btnBackward.setOnTouchListener(this);
	    btnRight.setOnTouchListener(this);
	    btnLeft.setOnTouchListener(this);
	    btnShoot.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN) {
			view.setPressed(true);
			switch(view.getId()) {
				// When a button is pressed, move the appropriate motor
				case R.id.btn_fwd: 
					NxtControl.forward();
					break;
				case R.id.btn_back: 
					NxtControl.backward();
					break;
				case R.id.btn_right: 
					NxtControl.right();
					break;
				case R.id.btn_left: 
					NxtControl.left();
					break;
				case R.id.btn_shoot: 
					NxtControl.shoot();
					break;
				default: return false;
			}
			
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_UP) {
			view.setPressed(false);
			// Stop all motors when nothing is being pressed
			NxtControl.stop();
			return true;
		}
		return false;
	}
	
    
}
