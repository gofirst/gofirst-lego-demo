package org.mngofirst.lego;

import java.io.IOException;
import java.io.InputStream;
//import java.util.UUID;
import java.io.OutputStream;
import java.lang.reflect.Method;

import android.bluetooth.*;
import android.util.Log;

/**
 * BluetoothThread
 * Thread that handles bluetooth communications with the NXT.
 * @author Bryan Herbst
 *
 */
public class BluetoothThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
   // private final UUID MY_UUID = UUID.randomUUID();
    private OutputStream btOut;
    private InputStream btIn;
    private volatile static BluetoothThread btThread;
    
    private static BluetoothMsgListener listener;

    public BluetoothThread(BluetoothDevice device) {
        // Use a temporary object that is later assigned to mmSocket,
        // because mmSocket is final
        BluetoothSocket tmp = null;
        mmDevice = device;

        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            // MY_UUID is the app's UUID string, also used by the server code
            Method m = mmDevice.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
            tmp = (BluetoothSocket) m.invoke(device, 1);
        } catch (Exception e) { }
        mmSocket = tmp;
        
        btThread = this;
    }

    public void run() {
    	try {
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            mmSocket.connect();
            btOut = mmSocket.getOutputStream();
            btIn = mmSocket.getInputStream();
            byte[] buffer;
            int bytes;
            while (true) {
            	bytes = btIn.available();
            	if(bytes > 0) {
            		buffer = new byte[bytes];
            		btIn.read(buffer);
            		String msg = "";
            		for(Byte b: buffer) {
            			msg += String.format("%02x", b) + ", ";
            		}
            		if (listener != null) 
            			listener.onMessageReceived(buffer);
            		Log.d("Bluetooth msg received", msg);
            	}
            	sleep(500);
            }
        } catch (IOException connectException) {
            // Unable to connect; close the socket and get out
            try {
                mmSocket.close();
                Log.d("GRRR", "----EXCEPTION----" + connectException);
            } catch (IOException closeException) { }
            return;
        } catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

    /** Will cancel an in-progress connection, and close the socket */
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) { }
    }
    
    /**
     * Write data to the bluetooth socket (and hopefully the NXT).
     * @param buffer
     */
    public void write(byte[] buffer) {
    	if(btOut != null) {
	        try {
	            btOut.write(buffer);
	            String msg = "";
        		for(Byte b: buffer) {
        			msg += String.format("%02x", b) + ", ";
        		}
        		Log.d("Bluetooth msg sent", msg);
        		
	        } catch (IOException e) {
	            Log.e("ERROR", "Exception during write", e);
	        }
    	}
    }
    
    public static synchronized BluetoothThread getThread() {
    	return btThread;
    }
    
    public static void setBluetoothMsgListener(BluetoothMsgListener bluetoothListener) {
    	listener = bluetoothListener;
    }
    
    public interface BluetoothMsgListener {
    	public void onMessageReceived(byte[] msg);
    }
}