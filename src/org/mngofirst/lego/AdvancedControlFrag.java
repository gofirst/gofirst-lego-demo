package org.mngofirst.lego;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

/**
 * Advanced NXT control Fragment.
 * @author Bryan Herbst
 *
 */
public class AdvancedControlFrag extends Fragment implements OnTouchListener, OnSeekBarChangeListener {
	private static final int NEUTRAL = Command.OUT_MAX;
	private static final int MULTIPLIER = 3;
	
	Button btnShoot;
	VerticalSeekBar seekLeft, seekRight;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return inflater.inflate(R.layout.adv_control_frag, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Get button views
	    btnShoot = (Button)getView().findViewById(R.id.btn_shoot);
	    seekLeft = (VerticalSeekBar)getView().findViewById(R.id.seekbar_left);
	    seekRight = (VerticalSeekBar)getView().findViewById(R.id.seekbar_right);
	    
	    // Set listeners
	    btnShoot.setOnTouchListener(this);
	    seekLeft.setOnSeekBarChangeListener(this);
	    seekRight.setOnSeekBarChangeListener(this);
	    
	    // Set default, max, and min values for seekbars
	    seekLeft.setMax(2*NEUTRAL);
	    seekRight.setMax(2*NEUTRAL);
	    seekLeft.setProgressAndThumb(NEUTRAL);
	    seekRight.setProgressAndThumb(NEUTRAL);
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN) {
			view.setPressed(true);
			switch(view.getId()) {
				// When a button is pressed, move the appropriate motor
				case R.id.btn_shoot: 
					NxtControl.shoot();
					break;
				default: return false;
			}
			
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_UP) {
			view.setPressed(false);
			// Stop all motors when nothing is being pressed
			NxtControl.stop();
			return true;
		}
		return false;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		// When a seekBar changes progress, set the drive motors
		int power;
		if (progress >= NEUTRAL) {
			power = Math.min((progress-NEUTRAL)*MULTIPLIER, Command.OUT_MAX);
		} else {
			power = Math.max((progress-NEUTRAL)*MULTIPLIER, Command.OUT_MIN);
		}
		switch(seekBar.getId()) {
			case R.id.seekbar_left: NxtControl.setLeftMotor(power); break;
			case R.id.seekbar_right: NxtControl.setRightMotor(power); break;
			default: break;
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// When a seekBar is no longer beeing held, stop the motor by setting progress to NEUTRAL
		switch(seekBar.getId()) {
			case R.id.seekbar_left: seekLeft.setProgressAndThumb(NEUTRAL); break;
			case R.id.seekbar_right: seekRight.setProgressAndThumb(NEUTRAL); break;
			default: break;
		}
	}
	
    
}
